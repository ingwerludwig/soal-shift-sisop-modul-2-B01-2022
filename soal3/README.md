## Soal 1

Mendownload 2 zip dari google drive kemudian di ekstrak dan file hasil extract di-decode dan hasil decode dikumpulkan dalam file txt berdasarkan nama foldernya. Kemudian hasil dari decode yang ada di dalam folder, di zip dan diberi password 'mihinomenest[Nama user]'

## Jawaban 1a
Download zip dan extract hasil zipnya di folder terpisah

Buat thread buat download
pthread_create(&(tid1), NULL, &download, NULL); //Download zip
```shell
pthread_create(&(tid1), NULL, &download, NULL); //Download zip
```
```shell
void* download(void *arg){
    statusz = 0;
    unsigned long i=0;
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid1)) //download
	{
        download_drive("1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", HOME "/music.zip");
        download_drive("1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", HOME "/quote.zip");   
	}
    statusz=1;
    return NULL;
}
```
```shell
void download_drive(char *id, char *save){
    char *link = malloc(100);
    strcpy(link, "https://docs.google.com/uc?export=download&id=");
    strcat(link, id);
   
    pid_t pid = fork();
    int status;
    if (pid == 0){
        char *argv[] = {"wget", "--quiet", "--no-check-certificate", link, "-O", save, NULL};
        execv("/opt/homebrew/bin/wget", argv);
        exit(0);
    }
    wait(&status);
}
```
Thread download memanggil fungsi download_drive untuk mendownload file, kemudian untuk menunggu file didownload (sebelumnya set statusz=0), setelah selesai set statusz=1 untuk melanjutkan di step unzip.

```shell
pthread_create(&(tid2), NULL, &extract, NULL); //Extract music
pthread_create(&(tid3), NULL, &extract, NULL); //Extract quote
```
```shell
void* extract(void *arg){

    while(statusz != 1)
    {

    }

    unsigned long i=0;
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid2)) //extract
    {

        createDir("music");
        extract_file(HOME "/music.zip", "music");

        
    }else if(pthread_equal(id,tid3)) //extract
    {
        createDir("quote");
        extract_file(HOME "/quote.zip", "quote");
        
    }
    return NULL;
}
```
```shell
void createDir(char route[]){
    pid_t pid = fork();
    int status;
    if (pid == 0){
        execlp("mkdir", "mkdir", "-p", route, NULL);
    }
    waitpid(pid,NULL,0);
}
```
while statusz != 1 digunakan untuk menunuggu file agar bisa diextract setelah di download, untuk mengekstrak filenya. Setelah itu, jalankan fungsi createDir untuk membuat directory dan jalankan fungsi extract_file dengan path yang sesuai dengan tid threadnya.  Pthread dengan tid2 digunakan untuk mendownload zip music. Pthread dengan tid3 digunakan untuk mendownload zip quote. Kemudian agar dapat jalan bersamaan, maka pthread_join harus digunakan. 

## Jawaban 1b

Decode file txt dengan base64 dan kumpulkan semua hasil decodenya kedalam file txt yang terpisah sesuai dengan nama foldernya

```shell
pthread_create(&(tid4), NULL, &decode, NULL); //decode 1
pthread_create(&(tid5), NULL, &decode, NULL); //decode 2
```
```shell
void* decode(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid4)) //decode 1
	{
        for(int i=0 ; i<9 ; i++){
   
            char *file_name = (char*) malloc(100);
            char *nomor = (char*)malloc(100); //convert nomor to string
            char *output_name = (char*)malloc(100); //initialize output name
            sprintf(nomor,"%d",i+1); 

            // full path copied to file_name -- file_name/m -- file_name/m1
            strcpy(file_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/"); strcat(file_name,"m"); strcat(file_name,nomor); 
            
            // file_name copied to output_name
            strcpy(output_name,file_name);

            // output_name.txt -- file_name/m1.txt
            strcat(file_name,".txt");

            // file_name/m11 -- file_name/m11.txt
            strcat(output_name,nomor);strcat(output_name,".txt");
            decode_file(file_name,output_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/music.txt");
        }
       
	}else if (pthread_equal(id,tid5)){ //decode 2
        for(int i=0 ; i<9 ; i++){
            char *file_name = (char*) malloc(100);
            char *nomor = (char*) malloc(100);
            char *output_name = (char*)malloc(100);
            sprintf(nomor,"%d",i+1);
            strcpy(file_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/quote/"); strcat(file_name,"q"); strcat(file_name,nomor); 
            strcpy(output_name,file_name);
            strcat(file_name,".txt");
            strcat(output_name,nomor);strcat(output_name,".txt");
            decode_file(file_name,output_name, "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/quote/quote.txt");
        }
    } 
    return NULL;
}
```
Jadi, kita melakukan looping sebanyak 9 kali (sebanyak hasil extract yang ada), kemudian inisialisasi string untuk membuat nama file dan dengan mengstrcat dan strcpy, kita dapat membentuk string yang dapat dipakai untuk path
```shell
 // full path copied to file_name -- file_name/m -- file_name/m1
            strcpy(file_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/"); strcat(file_name,"m"); strcat(file_name,nomor); 
            
            // file_name copied to output_name
            strcpy(output_name,file_name);

            // output_name.txt -- file_name/m1.txt
            strcat(file_name,".txt");

            // file_name/m11 -- file_name/m11.txt
            strcat(output_name,nomor);strcat(output_name,".txt");
```
```shell
decode_file(file_name,output_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/music.txt");
```
Kemudian jalankan fungsi decode_filenya, cara yang sama dilakukan untuk mendecode file yang lain.

## Jawaban 1c
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
```shell
createDir("/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/hasil");
    rename("/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/music.txt", "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/hasil/music.txt");
    rename("/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/quote/quote.txt", "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/hasil/quote.txt");
```
Setelah membuat directory dengan createDir, maka kita memanggil fungsi rename dari bawaan. Fungsi rename sendiri secara tidak langsung dapat memindahkan file sesuai dengan path yang kita tentukan (berisi 2 parameter)

## Jawaban 1d
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
```shell
pid_t pid = fork();
    if(pid == 0){
        char* argv[] = {"zip","-P","mihinomenestingwer","-r","hasil.zip","hasil",NULL};
        execv("/usr/bin/zip", argv);
    }
waitpid(pid,NULL,0);
```
Untuk mengzip foder hasil yang telah terbentuk, kita dapat menggunakan execv("/usr/bin/zip", argv); dan mempassword zip tersebut  {"zip","-P","mihinomenestingwer","-r","hasil.zip","hasil",NULL};
