#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<dirent.h>
#include<sys/stat.h>

char directory[] = "/Users/ingwerludwig/modul2";

char *strremove(char *str, const char *sub) {
    char *p, *q, *r;
    if (*sub && (q = r = strstr(str, sub)) != NULL) {
        size_t len = strlen(sub);
        while ((r = strstr(p = r + len, sub)) != NULL) {
            memmove(q, p, r - p);
            q += r - p;
        }
        memmove(q, p, strlen(p) + 1);
    }
    return str;
}

int partial_compare(char *str1, char *str2){
  int j=0,i=0;
  
  while(str2[j] != '\0'){
    if(str1[i] == '\0'){  return 1;}
      if(str1[i] == str2[j]){
        j++;i++;
        continue;
      }
      j++;i=0;
  }
  return 0;
}


int main(){
  pid_t child_id;

  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  if(child_id == 0){
      char *argv[]={"mkdir","-p","darat",directory,NULL};
      execv("/bin/mkdir",argv);
  }
  else{
      waitpid(child_id,NULL,0);
  }

  // Delay 3 secs
  sleep(3);
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  if(child_id == 0){
      char *argv[]={"mkdir","-p","air",directory,NULL};
      execv("/bin/mkdir",argv);
  }
  else{
      waitpid(child_id,NULL,0);
  }

  //Delay 3 secs
  sleep(3);
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  if(child_id == 0){
      char *argv[]={"unzip","-q","animal.zip",NULL}; //Unzip animal.zip
      execv("/usr/bin/unzip",argv);
  }
  else{
      waitpid(child_id,NULL,0);
  }
  
  char *dirtemp;
  dirtemp = (char*)malloc(sizeof(char)*(strlen(directory)+strlen("/animal.zip")));
  strcpy(dirtemp,directory);
  strcat(dirtemp,"/animal");
  DIR *dp;
  struct dirent *entry;
  struct stat statbuf;

  if((dp = opendir(dirtemp)) == NULL) {
    fprintf(stderr,"cannot open directory: %s\n", directory);    
  }

  chdir(dirtemp);

  //Read directory
  while((entry = readdir(dp)) != NULL) {
      lstat(entry->d_name,&statbuf);
      if(S_ISDIR(statbuf.st_mode)) {

          /* Found a directory, but ignore . and .. */
          if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0) continue;
      }else{

        //Checking Darat Animal
        if(partial_compare("darat", entry->d_name)){  
              //  Bird animals in darat removed
              if(strstr(entry->d_name,"bird")){
                remove(entry->d_name);
              }

              child_id = fork();
              if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
              }
              if(child_id == 0){
                char *dirtemp2;
                dirtemp2 = (char*)malloc(sizeof(char)*(strlen(directory)+strlen("/darat"))); //Make darat directory from current folder
                strcpy(dirtemp2,directory);
                strcat(dirtemp2,"/darat");

                char *argv[]={"mv",entry->d_name,dirtemp2,NULL};
                execv("/bin/mv",argv);
              }
              else{
                  waitpid(child_id,NULL,0);
              }
        }

        //Checking Air Animal
        else if (partial_compare("air", entry->d_name)){
              child_id = fork();

              if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
              }
              if(child_id == 0){
                char *dirtemp2;
                dirtemp2 = (char*)malloc(sizeof(char)*(strlen(directory)+strlen("/air"))); //Make air directory from current folder
                strcpy(dirtemp2,directory);
                strcat(dirtemp2,"/air");
                char *argv[]={"mv",entry->d_name,dirtemp2,NULL};
                execv("/bin/mv",argv);
              }
              else{
                  waitpid(child_id,NULL,0);
              }
              //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

              // Get UID and permission and write the filename to txt
              child_id = fork();
              if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
              }
              if (child_id == 0) {
                char *argv[] = {"touch", "/Users/ingwerludwig/modul2/air/list.txt", NULL}; // List.txt
                execv("/usr/bin/touch", argv);
              } else{
                  waitpid(child_id,NULL,0);
              }

              //Wrtie to list.txt
              child_id = fork();
              char fixed[100];
              if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
              }
              if (child_id == 0) {
                FILE *filetxt;
                filetxt = fopen("/Users/ingwerludwig/modul2/air/list.txt", "a");
                if(filetxt == NULL){
                   exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                }
                strcpy(fixed, entry->d_name);
                strremove(fixed,"_air");

                fputs(fixed, filetxt);
                fputs("\n", filetxt);

              } else{
                  waitpid(child_id,NULL,0);
              }
        }
        //Remove the other file that aren't air and darat animal
        else{
          remove(entry->d_name);  //Remove the other file that aren't air and darat animal
        }
          printf("%s\n",entry->d_name); //Print the name of the file
      }
  }
    closedir(dp);
  remove(dirtemp);
}